package com.example.ca2000;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Example local unit test, which will performCalculus on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class ExampleUnitTest {
    @Test
    public void addition_isCorrect() throws Exception {
        assertEquals(4, 2 + 2);
    }
}