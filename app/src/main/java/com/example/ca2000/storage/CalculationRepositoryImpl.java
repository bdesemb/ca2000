package com.example.ca2000.storage;

import com.example.ca2000.domain.model.Calculation;
import com.example.ca2000.domain.repository.CalculationRepository;

import java.util.List;

public class CalculationRepositoryImpl implements CalculationRepository {

    @Override
    public void insert(Calculation calculation) {

    }

    @Override
    public void update(Calculation calculation) {

    }

    @Override
    public Calculation getCalculationById(long id) {
        return null;
    }

    @Override
    public List<Calculation> getAllCalculations() {
        return null;
    }

    @Override
    public void delete(Calculation calculation) {

    }
}
