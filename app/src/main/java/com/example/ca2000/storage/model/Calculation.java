package com.example.ca2000.storage.model;

import io.requery.Entity;
import io.requery.Generated;
import io.requery.Key;
import io.requery.Persistable;

@Entity
public interface Calculation extends Persistable {

    @Key
    @Generated
    int getId();

    String getCalculation();
    void setCalculation(String calculation);

    double getResult();
    void setResult(double result);
}
