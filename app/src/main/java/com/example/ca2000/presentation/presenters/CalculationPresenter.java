package com.example.ca2000.presentation.presenters;

import com.example.ca2000.domain.executor.Executor;
import com.example.ca2000.domain.executor.MainThread;
import com.example.ca2000.domain.interactors.CalculationInteractor;
import com.example.ca2000.domain.interactors.impl.CalculationInteractorImpl;
import com.example.ca2000.domain.repository.CalculationRepository;

/**
 * Created by Benoit Desemberg on 10/05/2017.
 */

public class CalculationPresenter extends AbstractPresenter implements BasePresenter,
        CalculationInteractor.Callback {

    private static final String TAG = "CalculationPresenter";
    private final MvpView view;
    private final CalculationRepository calculationRepository;
    private StringBuilder calculus;
    private boolean isResult;

    public CalculationPresenter(Executor executor, MainThread mainThread,
                                MvpView view, CalculationRepository calculationRepository) {
        super(executor, mainThread);
        this.view = view;
        this.calculationRepository = calculationRepository;
        calculus = new StringBuilder();
    }

    @Override
    public void resume() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void stop() {

    }

    @Override
    public void destroy() {

    }

    @Override
    public void onError(String message) {

    }

    public void performCalculus() {
        CalculationInteractor calculationInteractor = new CalculationInteractorImpl(
                mExecutor,
                mMainThread,
                calculationRepository,
                this,
                calculus.toString()
        );
        calculationInteractor.execute();
    }

    public void onBtnClick(String s) {
        if (isResult) {
            calculus = new StringBuilder();
            isResult = false;
        }
        calculus.append(s);
        view.display(calculus.toString());
    }

    @Override
    public void onCalculationDone(Double result) {
        calculus = new StringBuilder(String.valueOf(result));
        view.display(calculus.toString());
        isResult = true;
    }

    @Override
    public void onError() {

    }

    public void del() {
        if (calculus.length() != 0) {
            calculus.deleteCharAt(calculus.length() - 1);
        }
        view.display(calculus.toString());
    }

    public interface MvpView {

        void display(String s);
    }
}
