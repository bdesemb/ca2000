package com.example.ca2000.presentation.ui.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.example.ca2000.R;
import com.example.ca2000.domain.executor.impl.MainThreadImpl;
import com.example.ca2000.domain.executor.impl.ThreadExecutor;
import com.example.ca2000.presentation.presenters.CalculationPresenter;
import com.example.ca2000.storage.CalculationRepositoryImpl;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Benoit Desemberg on 10/05/2017.
 */

public class CalculationFragment extends Fragment implements CalculationPresenter.MvpView {
    private static final String TAG = "CalculationFragment";
    @BindView(R.id.btn_cal_0)
    Button btn0;
    @BindView(R.id.btn_cal_1)
    Button btn1;
    @BindView(R.id.btn_cal_2)
    Button btn2;
    @BindView(R.id.btn_cal_3)
    Button btn3;
    @BindView(R.id.btn_cal_4)
    Button btn4;
    @BindView(R.id.btn_cal_5)
    Button btn5;
    @BindView(R.id.btn_cal_6)
    Button btn6;
    @BindView(R.id.btn_cal_7)
    Button btn7;
    @BindView(R.id.btn_cal_8)
    Button btn8;
    @BindView(R.id.btn_cal_9)
    Button btn9;
    @BindView(R.id.btn_cal_pt)
    Button btnPt;
    @BindView(R.id.btn_cal_division)
    Button btnDiv;
    @BindView(R.id.btn_cal_minus)
    Button btnMinus;
    @BindView(R.id.btn_cal_plus)
    Button btnPlus;
    @BindView(R.id.btn_cal_mult)
    Button btnMult;
    @BindView(R.id.btn_cal_equals)
    Button btnEquals;
    @BindView(R.id.btn_cal_del)
    Button btnDel;
    @BindView(R.id.tv_display)
    TextView tvDisplay;


    private List<Button> buttons;
    private CalculationPresenter calculationPresenter;

    public CalculationFragment() {
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        calculationPresenter = new CalculationPresenter(
                ThreadExecutor.getInstance(),
                MainThreadImpl.getInstance(),
                this,
                new CalculationRepositoryImpl());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_calculation, container, false);
        ButterKnife.bind(this, view);
        onBind();
        for (int i = 0; i < buttons.size(); i++) {
            buttons.get(i).setOnClickListener(v -> calculationPresenter.onBtnClick(
                    ((Button)v).getText().toString()));
        }
        btnEquals.setOnClickListener(v -> calculationPresenter.performCalculus());
        //Replace special characters
        btnMult.setOnClickListener(v -> calculationPresenter.onBtnClick(("*")));
        btnDiv.setOnClickListener(v -> calculationPresenter.onBtnClick(("/")));
        btnDel.setOnClickListener(v -> calculationPresenter.del());
        return view;
    }

    private void onBind() {
        buttons = new ArrayList<>();
        buttons.add(btn0);
        buttons.add(btn1);
        buttons.add(btn2);
        buttons.add(btn3);
        buttons.add(btn4);
        buttons.add(btn5);
        buttons.add(btn6);
        buttons.add(btn7);
        buttons.add(btn8);
        buttons.add(btn9);
        buttons.add(btnPt);
        buttons.add(btnMinus);
        buttons.add(btnPlus);
    }

    private void onBoutonClick(CharSequence btnText) {
        calculationPresenter.onBtnClick(btnText.toString());
    }

    @Override
    public void display(String s) {
        tvDisplay.setText(s);
    }
}
