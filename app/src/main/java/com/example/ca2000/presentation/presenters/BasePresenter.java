package com.example.ca2000.presentation.presenters;

public interface BasePresenter {
    void resume();

    void pause();

    void stop();

    void destroy();

    void onError(String message);
}
