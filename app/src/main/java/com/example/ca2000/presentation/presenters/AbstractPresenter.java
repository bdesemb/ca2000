package com.example.ca2000.presentation.presenters;

import com.example.ca2000.domain.executor.Executor;
import com.example.ca2000.domain.executor.MainThread;

public abstract class AbstractPresenter {

    protected Executor   mExecutor;
    protected MainThread mMainThread;

    public AbstractPresenter(Executor executor, MainThread mainThread) {
        mExecutor = executor;
        mMainThread = mainThread;
    }
}
