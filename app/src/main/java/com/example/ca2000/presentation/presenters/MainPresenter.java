package com.example.ca2000.presentation.presenters;

import com.example.ca2000.domain.executor.Executor;
import com.example.ca2000.domain.executor.MainThread;

public class MainPresenter extends AbstractPresenter {

    public MainPresenter(Executor executor, MainThread mainThread) {
        super(executor, mainThread);
    }
}
