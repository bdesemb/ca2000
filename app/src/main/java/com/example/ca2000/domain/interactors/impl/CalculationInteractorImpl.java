package com.example.ca2000.domain.interactors.impl;

import android.util.Log;

import com.example.ca2000.domain.executor.Executor;
import com.example.ca2000.domain.executor.MainThread;
import com.example.ca2000.domain.interactors.CalculationInteractor;
import com.example.ca2000.domain.interactors.base.AbstractInteractor;
import com.example.ca2000.domain.model.Calculation;
import com.example.ca2000.domain.repository.CalculationRepository;
import com.example.ca2000.util.ReversePolishNotation;

import java.util.*;
import java.util.regex.Pattern;

public class CalculationInteractorImpl extends AbstractInteractor implements CalculationInteractor {

    private static final String TAG = "CalculationInteractor";
    private final String calculation;
    private final Callback callback;
    private final CalculationRepository repository;
    private Stack<Double> stack;

    public CalculationInteractorImpl(Executor threadExecutor, MainThread mainThread,
                                     CalculationRepository repo, Callback callback,
                                     String calculation) {
        super(threadExecutor, mainThread);

        if (repo == null || callback == null) {
            throw new IllegalArgumentException("Arguments can not be null!");
        }

        this.callback = callback;
        this.repository = repo;
        this.calculation = calculation;
    }

    @Override
    public void run() {
        List<String> calc = new ArrayList<>();
        //String[] calc = new String[calculation.length()];
        int counter = 0;
        StringBuilder numStr = new StringBuilder();
        while(counter < calculation.length()) {
            char c = calculation.charAt(counter);
            if (Character.isDigit(c) || c == '.') {
                numStr.append(c);
            } else {
                calc.add(numStr.toString());
                numStr = new StringBuilder();
                calc.add(String.valueOf(c));
            }
            counter++;
        }
        calc.add(numStr.toString());
        Log.d(TAG, "run: " + calc);
        String[] rpn = ReversePolishNotation.infixToRPN(calc.toArray(new String[calc.size()]));

        stack = new Stack<>();
        for (String ch : rpn) {
            if (Objects.equals(ch, "*")) {
                stack.push(stack.pop() * stack.pop());
            } else if (Objects.equals(ch, "/")) {
                double op2 = stack.pop();
                double op1 = stack.pop();
                stack.push(op1 / op2);
            } else if (Objects.equals(ch, "+")) {
                stack.push(stack.pop() + stack.pop());
            } else if (Objects.equals(ch, "-")) {
                double op2 = stack.pop();
                double op1 = stack.pop();
                stack.push(op1 - op2);
            } else {
                stack.push(parseNumber(ch));
            }
        }
        double result = stack.pop();
        repository.insert(new Calculation(calculation, result));
        mMainThread.post(() -> callback.onCalculationDone(stack.pop()));
    }

    //From Double.valueOf javadoc
    private double parseNumber(String s) {
        final String Digits = "(\\p{Digit}+)";
        final String HexDigits = "(\\p{XDigit}+)";
        // an exponent is 'e' or 'E' followed by an optionally
        // signed decimal integer.
        final String Exp = "[eE][+-]?" + Digits;
        final String fpRegex =
                ("[\\x00-\\x20]*" +  // Optional leading "whitespace"
                        "[+-]?(" + // Optional sign character
                        "NaN|" +           // "NaN" string
                        "Infinity|" +      // "Infinity" string

                        // A decimal floating-point string representing a finite positive
                        // number without a leading sign has at most five basic pieces:
                        // Digits . Digits ExponentPart FloatTypeSuffix
                        //
                        // Since this method allows integer-only strings as input
                        // in addition to strings of floating-point literals, the
                        // two sub-patterns below are simplifications of the grammar
                        // productions from section 3.10.2 of
                        // The Java Language Specification.

                        // Digits ._opt Digits_opt ExponentPart_opt FloatTypeSuffix_opt
                        "(((" + Digits + "(\\.)?(" + Digits + "?)(" + Exp + ")?)|" +

                        // . Digits ExponentPart_opt FloatTypeSuffix_opt
                        "(\\.(" + Digits + ")(" + Exp + ")?)|" +

                        // Hexadecimal strings
                        "((" +
                        // 0[xX] HexDigits ._opt BinaryExponent FloatTypeSuffix_opt
                        "(0[xX]" + HexDigits + "(\\.)?)|" +

                        // 0[xX] HexDigits_opt . HexDigits BinaryExponent FloatTypeSuffix_opt
                        "(0[xX]" + HexDigits + "?(\\.)" + HexDigits + ")" +

                        ")[pP][+-]?" + Digits + "))" +
                        "[fFdD]?))" +
                        "[\\x00-\\x20]*");// Optional trailing "whitespace"

        if (Pattern.matches(fpRegex, s))
             return Double.valueOf(s); // Will not throw NumberFormatException
        else {
            callback.onError();
        }
        return 0;
    }
}
