package com.example.ca2000.domain.interactors;

import com.example.ca2000.domain.interactors.base.Interactor;

public interface CalculationInteractor extends Interactor {
    interface Callback {
        void onCalculationDone(Double result);
        void onError();
    }
}
