package com.example.ca2000.domain.repository;

import com.example.ca2000.domain.model.Calculation;

import java.util.List;

public interface CalculationRepository {

    void insert(Calculation calculation);

    void update(Calculation calculation);

    Calculation getCalculationById(long id);

    List<Calculation> getAllCalculations();

    void delete(Calculation calculation);
}
