package com.example.ca2000.domain.model;

public class Calculation {

    private String operation;
    private double result;
    private int id;

    public Calculation(String operation, double result) {
        this.operation = operation;
        this.result = result;
    }

    public Calculation(int id, String operation, double result) {
        this.id = id;
        this.operation = operation;
        this.result = result;
    }

    public String getOperation() {
        return operation;
    }

    public void setOperation(String operation) {
        this.operation = operation;
    }

    public double getResult() {
        return result;
    }

    public void setResult(double result) {
        this.result = result;
    }
}
