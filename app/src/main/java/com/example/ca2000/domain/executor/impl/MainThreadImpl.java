package com.example.ca2000.domain.executor.impl;

import android.os.Handler;
import android.os.Looper;

import com.example.ca2000.domain.executor.MainThread;

/**
 * Created by Benoit Desemberg on 10/05/2017.
 */

public class MainThreadImpl implements MainThread {
    private static MainThread sMainThread;

    private Handler mHandler;

    private MainThreadImpl() {
        mHandler = new Handler(Looper.getMainLooper());
    }

    @Override
    public void post(Runnable runnable) {
        mHandler.post(runnable);
    }

    public static MainThread getInstance() {
        if (sMainThread == null) {
            sMainThread = new MainThreadImpl();
        }

        return sMainThread;
    }
}
